package ir.cps.wifidirect;

import android.os.AsyncTask;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


public class ServerAsync extends AsyncTask<String, String, String> {


    private MainActivity activity;

    private String clientName;
    private StringBuilder finalRes = new StringBuilder();
    private ArrayList<Socket> clients = new ArrayList<>();


    private Boolean checkClientExist(Socket socket){
        Boolean exist = false;
        for(Socket s : this.clients){
            if(s.getInetAddress().getHostName().equals(socket.getInetAddress().getHostName())){
                exist = true;
            }
        }
        return exist;
    }

    public ServerAsync(MainActivity activity) {
        this.activity = activity;
    }


    @Override
    protected String doInBackground(String[] params) {

        try {
            ServerSocket serverSocket = new ServerSocket(8081);
            while (activity.serverCreate) {
                Socket socket = serverSocket.accept();


                if(checkClientExist(socket)) {
                    this.activity.clientsSocket.remove(socket);
                }
                this.activity.clientsSocket.add(socket);

            }
            serverSocket.close();
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return "";
    }


}

