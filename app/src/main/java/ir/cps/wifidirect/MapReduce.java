package ir.cps.wifidirect;

import java.util.ArrayList;
import java.util.List;

public class MapReduce {

    public ArrayList<List<Integer>> prepareMapData(ArrayList<Integer> vector){
        ArrayList<List<Integer>> data = new ArrayList<>();
        for(int i=0; i<vector.size(); i = i+4){
            data.add(vector.subList(i,i+4));
        }
        return data;
    }

    public ArrayList<ArrayList<Integer>> mapFunction(ArrayList<Integer> vector, ArrayList<List<Integer>> input){
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            ArrayList<Integer> temp = new ArrayList<>();
            for(int j=0; j< vector.size();j++){
                Integer res = vector.get(j) * input.get(i).get(j);
                temp.add(res);
            }
            result.add(temp);
        }
        return result;
    }

    public ArrayList<Integer> reduceFunction(ArrayList<ArrayList<Integer>> vector){
        ArrayList<Integer> results = new ArrayList<>();
        for(int i = 0; i < vector.size(); i++){
            Integer sum = 0;
            for(int j=0; j< vector.get(i).size(); j++)
                sum += vector.get(i).get(j);
            results.add(sum);
        }
        return results;
    }

}
