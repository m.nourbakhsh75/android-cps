package ir.cps.wifidirect;


public class ConvolutionalPartitioner {
    private int sigma(int[] inputArray, int size) {
        int result = 0;
        for(int i = 0; i < size; i++)
            result += inputArray[i];
        return result;
    }

    public int[][] partition(int numOfRows, int numOfColumns, int numOfWorkerNodes, int filterSize, int[] computingCaps) {
        int[] init = new int [numOfWorkerNodes];
        int[][] workLoad = new int [numOfWorkerNodes][2];
        int m = (numOfRows > numOfColumns) ? numOfRows : numOfColumns;
        int totalComputingCaps = sigma(computingCaps, computingCaps.length);
        int CAi = 0;
        for(int i = 0; i < numOfWorkerNodes; i++) {
            init[i] = CAi * (m - (filterSize - 1)) / totalComputingCaps;
            CAi += computingCaps[i];
        }
        for(int i = 0; i < numOfWorkerNodes; i++) {
            workLoad[i][0] = init[i];
            if(i != numOfWorkerNodes - 1)
                workLoad[i][1] = init[i + 1] + filterSize - 2;
            else
                workLoad[i][1] = m - 1;
        }

        return workLoad;
    }
}
