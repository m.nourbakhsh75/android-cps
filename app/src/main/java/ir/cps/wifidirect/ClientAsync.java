package ir.cps.wifidirect;

import android.os.AsyncTask;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.*;


public class ClientAsync extends AsyncTask<String, String, String> {

    private ArrayList<Integer> serverVector = new ArrayList<>();
    private String host;
    private MapReduce mapReduce = new MapReduce();
    private ArrayList<Integer> mainVector = new ArrayList<>();

    private MainActivity activity;

    private void randomVector(){
        Random rand = new Random();
        ArrayList<Integer> v = new ArrayList<>();
        for (int i = 0; i<4; i++){
            v.add(rand.nextInt(10));
        }
        this.mainVector = v;
    }

    public ClientAsync(MainActivity activity, String host) {
        this.host = host;
        this.activity = activity;
        this.randomVector();

        StringBuilder message = new StringBuilder();
        for (int i = 0; i < this.mainVector.size(); i++) {
            if (i < this.mainVector.size() - 1)
                message.append(this.mainVector.get(i)).append(",");
            else
                message.append(this.mainVector.get(i));
        }

        this.activity.vectorTxt.setText("vector: [ " + message.toString() + " ]");
    }

    private String concatVector(ArrayList<Integer> vector) {
        StringBuilder message = new StringBuilder();
        for (int i = 0; i < vector.size(); i++) {
            if (i < vector.size() - 1)
                message.append(vector.get(i)).append(",");
            else
                message.append(vector.get(i));
        }
        return message.toString();
    }


    @Override
    protected String doInBackground(String[] params) {
        try {
            Socket socket = new Socket(this.host, 8081);
            System.out.println("Client is connected");

            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            System.out.println("Data streams are connected in the client side");

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String receivedVector = reader.readLine();
            System.out.println("received:" + receivedVector);

            StringTokenizer tokenizer = new StringTokenizer(receivedVector, ",");
            while (tokenizer.hasMoreTokens())
                serverVector.add(Integer.parseInt(tokenizer.nextToken()));

            ArrayList<List<Integer>> mapInputData = mapReduce.prepareMapData(serverVector);
            ArrayList<ArrayList<Integer>> mapOutputData = mapReduce.mapFunction(this.mainVector,mapInputData);
            ArrayList<Integer> results = mapReduce.reduceFunction(mapOutputData);
            String result = concatVector(results);
            System.out.println("result: " + result);
            System.out.println(mapOutputData.toString());
            String messageToSend = result + "\n";

            System.out.println("message to send: " + messageToSend);
            PrintWriter writer = new PrintWriter(outputStream);
            writer.write(messageToSend);
            writer.flush();

            reader.close();
            writer.close();
            socket.close();

            return receivedVector;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        this.activity.responseTxt.setVisibility(View.VISIBLE);
        this.activity.responseTxt.setText("vector from server : [ " + result + " ]");
    }

}
