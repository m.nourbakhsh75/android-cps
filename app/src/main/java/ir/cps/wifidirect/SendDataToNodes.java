package ir.cps.wifidirect;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class SendDataToNodes {

    private ArrayList<Socket> clients;
    private ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
    private MainActivity activity;
    private String result;
    private ConvolutionalPartitioner convolutionalPartitioner = new ConvolutionalPartitioner();

    public SendDataToNodes(ArrayList<Socket> client,MainActivity activity){
        this.clients = client;
        this.activity = activity;
        this.createMatrix();
    }

    private void createMatrix(){
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1,2,3,4));
        for(int i=0; i<4; i++){
            this.matrix.add(arrayList);
        }
    }

    private ArrayList<ArrayList<Integer>> partitioning(Integer partition){
        ArrayList<ArrayList<Integer>> partitions = new ArrayList<>();
        Integer length = this.matrix.size();
        Integer chunk = length/partition;
        for(int i= 0 ; i< length; i = i+chunk){
            ArrayList<Integer> part = new ArrayList<>();
            for(int j = i; j < i+chunk; j++ ){
                for(int k = 0; k<matrix.get(j).size(); k++)
                    part.add(matrix.get(j).get(k));
            }
            partitions.add(part);
        }
        return partitions;
    }

    public String getResult() {
        return result;
    }

    private String concatVector(ArrayList<Integer> vector) {
        StringBuilder message = new StringBuilder();
        for (int i = 0; i < vector.size(); i++) {
            if (i < vector.size() - 1)
                message.append(vector.get(i)).append(",");
            else
                message.append(vector.get(i));
        }
        return message.toString();
    }

    public void sending(){

        ArrayList<SendData> sendDataThread = new ArrayList<>();
        Integer partition = this.clients.size();

        ArrayList<ArrayList<Integer>> partitions = partitioning(partition);
        System.out.println(partition);
        for(int i = 0; i < partition; i++){
            String data = concatVector(partitions.get(i));
            SendData s = new SendData(clients.get(i),data);
            sendDataThread.add(s);
            s.start();
        }
        String g = "";
        for(SendData s: sendDataThread){
            try {
                s.join();
                g +=  s.getResult() + "\n" ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(g);
        this.activity.logText.setText(g);
        this.activity.responseTxt.setText(g);

    }


    class SendData extends Thread{
        private Socket socket;
        String result;
        String data;
        public SendData(Socket socket,String data){
            this.socket = socket;
            this.data = data;
        }
        public String getResult() {
            return result;
        }
        @Override
        public void run() {
            System.out.println(socket.getInetAddress().getHostName());
            try {
                OutputStream outputStream = socket.getOutputStream();
                InputStream inputStream = socket.getInputStream();
                System.out.println("Data streams are connected in the server side");

                PrintWriter writer = new PrintWriter(outputStream);
                writer.write(data + "\n");
                writer.flush();

                System.out.println("vector sent");

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String response = reader.readLine();
                System.out.println("response : " + response);

                result = response;

                writer.close();
                reader.close();
                socket.close();

            }catch (IOException i){
                System.out.println("EXCEPTION");
                System.out.println(i);
            }
        }

    }
}
