package ir.cps.wifidirect;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.Manifest;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.Build;
import android.os.Bundle;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.content.BroadcastReceiver;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class MainActivity extends AppCompatActivity implements WifiP2pManager.ChannelListener,
        WifiP2pManager.PeerListListener, WifiP2pManager.ConnectionInfoListener {

    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION = 1001;

    private WifiP2pManager manager;
    public boolean isWifiP2pEnabled = false;
    public boolean serverCreate = false;

    private Channel channel;
    private BroadcastReceiver receiver = null;
    private final IntentFilter intentFilter = new IntentFilter();

    List<WifiP2pDevice> devices = new ArrayList<>();
    ArrayList<Socket> clientsSocket = new ArrayList<>();
    Context context;

    Button connectBtn;
    Button disconnectBtn;
    Button discoverBtn;
    Button sendData;

    TextView statusTxt;
    TextView deviceTxt;
    TextView vectorTxt;
    TextView responseTxt;
    TextView logText;
    TextView responseText2;
    Button createGroup;
    ListView deviceList;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION:
                if  (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e("wifi-direct", "Fine location permission is not granted!");
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MainActivity.PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION);
            // After this point you wait for callback in
            // onRequestPermissionsResult(int, String[], int[]) overridden method
        }
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        connectBtn = findViewById(R.id.connect_btn);
        connectBtn.setVisibility(View.GONE);
        connectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                WifiP2pDevice device = devices.get(0);
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                System.out.println(device.deviceName);

                deviceTxt.setText("connecting ...");

                manager.connect(channel, config, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onFailure(int reason) {
                        Toast.makeText(MainActivity.this, "Connection failed. Retry.",
                                Toast.LENGTH_SHORT).show();
                        statusTxt.setText("peers found !");
                    }
                });
            }
        });

        disconnectBtn = findViewById(R.id.disconnect_btn);
        disconnectBtn.setVisibility(View.GONE);
        disconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverCreate = false;
                createGroup.setVisibility(View.VISIBLE);
                manager.removeGroup(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onFailure(int reasonCode) {
                        Log.d("wifi-direct", "Disconnect failed. Reason :" + reasonCode);
                    }

                    @Override
                    public void onSuccess() {
                    }

                });
            }
        });

        statusTxt = findViewById(R.id.status_txt);
        deviceTxt = findViewById(R.id.device_txt);
        vectorTxt = findViewById(R.id.vector_txt);
        logText = findViewById(R.id.logText);
        responseTxt = findViewById(R.id.response_txt);
        responseTxt.setVisibility(View.GONE);
        responseText2 = findViewById(R.id.response_txt2);
        responseText2.setVisibility(View.GONE);

        deviceList = findViewById(R.id.deviceList);

        discoverBtn = findViewById(R.id.discover_btn);
        discoverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusTxt.setText("discovering ...");
                initiateDiscovery();
            }
        });

        createGroup = findViewById(R.id.group);
        createGroup.setVisibility(View.VISIBLE);
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serverCreate = true;

                manager.createGroup(channel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        createGroup.setVisibility(View.GONE);
                        // Device is ready to accept incoming connections from peers.
                        Toast.makeText(MainActivity.this, "P2P group created",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reason) {
                        Toast.makeText(MainActivity.this, "P2P group creation failed. Retry.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        sendData = findViewById(R.id.sendBtn);
        sendData.setVisibility(View.GONE);
        sendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clientsSocket.size() > 0){
                    SendDataToNodes sendDataToNodes = new SendDataToNodes(clientsSocket,MainActivity.this);
                    sendDataToNodes.sending();
                }else{
                    deviceTxt.setText("No device connected ...");
                    Toast.makeText(MainActivity.this, "No device connected ...",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void initiateDiscovery() {
        this.manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Toast.makeText(MainActivity.this, "Discovery Initiated",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int reasonCode) {
                statusTxt.setText("tap Discover to find nearby peers");
                Toast.makeText(MainActivity.this, "Discovery Failed : " + reasonCode,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
        Toast.makeText(this, "Wifi P2P " + (isWifiP2pEnabled ? "enabled" : "disabled"),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void onChannelDisconnected() {
        Toast.makeText(this, "Channel lost.", Toast.LENGTH_LONG).show();
    }

    private static String getDeviceStatus(int deviceStatus) {
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";
        }
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        this.devices.clear();
        ArrayList<String> names = new ArrayList<>();
        for (WifiP2pDevice device : peers.getDeviceList()) {
            this.devices.add(device);
            System.out.println(device);
            if (device.isGroupOwner())
                names.add(device.deviceName);
            String status = getDeviceStatus(device.status);
            Log.d("wifi-direct", "Peer name :" + device.deviceName + ", status :" + status);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);

        TextView textView = new TextView(context);
        textView.setText("Devices:");
        this.deviceList.setAdapter(arrayAdapter);
        onListItemClick(this.devices);


        if (this.devices.size() > 0) {
            statusTxt.setText("Peer name :" + this.devices.get(0).deviceName );
            connectBtn.setVisibility(View.GONE);
        } else {
            statusTxt.setText("no peers found ...");
            connectBtn.setVisibility(View.GONE);
        }
    }

    public void connectToPeer(WifiP2pDevice device){

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        System.out.println(device.deviceName);

        deviceTxt.setText("connecting ...");

        manager.connect(channel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                createGroup.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(MainActivity.this, "Connection failed. Retry.",
                        Toast.LENGTH_SHORT).show();
                statusTxt.setText("peers found !");
                System.out.println(reason);
            }
        });
    }

    public void onListItemClick(final List<WifiP2pDevice> devices){

        this.deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                for (WifiP2pDevice device: devices){
                    if (device.deviceName.equals(selectedItem)){
                        connectToPeer(device);
                    }
                }
            }
        });

    }


    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        deviceTxt.setText("connected. " + (info.isGroupOwner ? "group owner" : info.groupOwnerAddress.getHostName()));
        disconnectBtn.setVisibility(View.VISIBLE);
        connectBtn.setVisibility(View.GONE);
        statusTxt.setVisibility(View.INVISIBLE);
        discoverBtn.setVisibility(View.INVISIBLE);

        Log.d("wifi-direct", "Group Owner IP - " + info.groupOwnerAddress.getHostAddress());
        connectBtn.setVisibility(View.GONE);

        if (info.groupFormed && info.isGroupOwner) {
            new ServerAsync(this).execute();
            sendData.setVisibility(View.VISIBLE);

        } else {
            new ClientAsync(this, info.groupOwnerAddress.getHostAddress()).execute();
        }
    }



    public void resetActivity() {
        this.devices.clear();

        statusTxt.setText("tap Discover to find nearby peers");
        deviceTxt.setText("No device connected ...");
        vectorTxt.setText("No vector yet ...");
        responseTxt.setText("");
        responseTxt.setVisibility(View.GONE);
        statusTxt.setVisibility(View.VISIBLE);

        discoverBtn.setVisibility(View.VISIBLE);
        disconnectBtn.setVisibility(View.INVISIBLE);
        connectBtn.setVisibility(View.GONE);
        disconnectBtn.setVisibility(View.GONE);
        sendData.setVisibility(View.GONE);
        createGroup.setVisibility(View.VISIBLE);
        serverCreate = false;
        clientsSocket.clear();

    }
}
